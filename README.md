# lug-wine

Buildinfo:
--

This is a Lutris runner for StarCitizen, but as there is no patches needed atm, its a unmodified wine-staging.

It is build with wine source control (https://github.com/rawfoxDE/wsc) using a podman container build based on Ubuntu18.
On first load of the game, the prefix get updated and may want you to install/reinstall wine-mono.
If offered, do so !

When you run into a black screen with StarCitizen, make sure you have vcrun2017/vcredist and d3dcompiler_47 installed in your prefix.
You may have to copy the d3dcompiler_47.dll from the launcher dir to the games /Bin64 folder.

On first run, the game is creating shaders and filling caches, resulting in bad game performance.
That will vanish over time, just keep playing it.

Important:
--
People reporting issues with symlinks in the ZIP download ! 
You want to git clone the runner to keep the symlinks inside the runner. To do that, you enter the following in your bash, i have the git clone in ~/src:

cd ~/src

git clone https://github.com/rawfoxDE/lug-wine.git ./lug-wine

That will clone the runner into /home/you/src/lug-wine

Installation
--
To use it under Lutris, copy or symlink the directory lug-runner-5.5 to your Lutris runners. 
You may find them under '/home/you/.local/share/lutris/runners/wine'.
After that, tweak your Lutris game configuration to set the runner 'lug-runner-5.5' as shown in the list. 

For help and support, join the Linux Users Group at CIG. 

https://robertsspaceindustries.com/spectrum/community/LUG/lobby/104

Find the main installation thread here:

https://robertsspaceindustries.com/spectrum/community/LUG/forum/149/thread/star-citizen-on-linux-information-thread-readme
